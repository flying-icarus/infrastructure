
all: venv python-deps

venv:
	virtualenv -p python2 venv

python-deps:
	. venv/bin/activate && pip install -r requirements.txt

.PHONY: all python-deps
